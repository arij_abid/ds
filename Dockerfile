FROM openjdk:7-jdk-alpine
WORKDIR /opt/app
COPY ./ /opt/app
RUN mvn clean install -DskipTests
COPY --from=build /opt/app/target/*.jar app.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","-Xmx1024M","-Dserver.port=${PORT}","app.jar"]
CMD ["sh", "-c", "run:app" ]


 